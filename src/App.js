import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Products from './pages/Products';
import Product from './pages/Product';
import Cart from './pages/Cart';
import Home from './pages/Home';

function App() {
	return (
		<Router>
			<div className="app">
				<Switch>
					<Route path="/" component={Home} exact />
					<Route path="/home" component={Home} />
					<Route path="/cart" component={Cart} />
					<Route path="/products" component={Products} />
					<Route path="/product/:id" component={Product} />
				</Switch>
			</div>
		</Router>
	);
}

export default App;
