import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import productApi from '../api/productApi';

export const fetchAllProducts = createAsyncThunk(
	'product/fetchAllProducts',
	async () => {
		const data = await productApi.fetchAllProducts();

		return data;
	}
);

export const productSlice = createSlice({
	name: 'product',
	initialState: {
		loading: false,
		error: null,
		products: [],
		totalProducts: null,
		searchedProduct: null,
	},
	reducers: {
		setSearchedProduct: (state, action) => {
			state.searchedProduct = action.payload;
		},
	},
	extraReducers: {
		[fetchAllProducts.pending]: state => {
			state.loading = true;
		},
		[fetchAllProducts.fulfilled]: (state, action) => {
			state.loading = false;
			state.products = action.payload.Products;
			state.totalProducts = action.payload.TotalProductsCount;
		},
		[fetchAllProducts.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
	},
});

export const { setSearchedProduct } = productSlice.actions;

export const productSelector = state => state.product;

export default productSlice.reducer;
