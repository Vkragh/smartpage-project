import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import cartApi from '../api/cartApi';

export const createNewCart = createAsyncThunk(
	'product/createNewCart',
	async () => {
		const cart = await cartApi.createCart();
		localStorage.setItem('cart_key', cart.Secret);
		return cart;
	}
);

export const fetchCart = createAsyncThunk('product/fetchCart', async () => {
	const secret = localStorage.getItem('cart_key');
	const data = await cartApi.fetchCart(secret);
	return data;
});

export const addProductToCart = createAsyncThunk(
	'product/addProductToCart',
	async product => {
		const secret = localStorage.getItem('cart_key');
		await cartApi.addItemToCart(secret, product);
	}
);

export const updateCartItem = createAsyncThunk(
	'product/updateCartItem',
	async productData => {
		const { id, quantity } = productData;
		const secret = localStorage.getItem('cart_key');
		await cartApi.updateCartItem(secret, id, quantity);
	}
);

export const clearCart = createAsyncThunk('product/clearCart', async () => {
	const secret = localStorage.getItem('cart_key');
	await cartApi.clearCart(secret);
});

export const cartSlice = createSlice({
	name: 'cart',
	initialState: {
		loading: false,
		error: null,
		cart: null,
		totalPrice: 0,
		productsCount: 0,
	},
	reducers: {},
	extraReducers: {
		[createNewCart.pending]: state => {
			state.loading = true;
		},
		[createNewCart.fulfilled]: state => {
			state.loading = false;
		},
		[createNewCart.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
		[addProductToCart.pending]: state => {
			state.loading = true;
		},
		[addProductToCart.fulfilled]: state => {
			state.loading = false;
		},
		[addProductToCart.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
		[updateCartItem.pending]: state => {
			state.loading = true;
		},
		[updateCartItem.fulfilled]: state => {
			state.loading = false;
		},
		[updateCartItem.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
		[clearCart.pending]: state => {
			state.loading = true;
		},
		[clearCart.fulfilled]: state => {
			state.loading = false;
		},
		[clearCart.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
		[fetchCart.pending]: state => {
			state.loading = true;
		},
		[fetchCart.fulfilled]: (state, action) => {
			state.loading = false;
			state.cart = action.payload;
			state.totalPrice = state.cart.OrderLines.reduce((acc, curr) => {
				return (acc += curr.Price.Price * curr.Quantity);
			}, 0);
			state.productsCount = state.cart.OrderLines.reduce((acc, curr) => {
				return (acc += curr.Quantity);
			}, 0);
		},
		[fetchCart.rejected]: (state, action) => {
			state.loading = false;
			state.error = action.payload;
		},
	},
});

export const cartSelector = state => state.cart;

export default cartSlice.reducer;
