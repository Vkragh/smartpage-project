import React from 'react';

function HomeShowcase() {
	return (
		<div className="container mb-5 home-showcase">
			<div className="row">
				<div className="col-lg-12 text-center py-5">
					<h6>Vælg en ægte Variant til arbejdet og få kvalitet på krogen</h6>
				</div>
				<div className="col-lg-6">
					<div className="case">
						<h4>Lorem ipsum dolor</h4>
						<p>
							Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
							ipsum
						</p>
						<button className="btn btn-secondary">Read more</button>
					</div>
				</div>
				<div className="col-lg-6">
					<div className="case-dark"></div>
				</div>
				<div className="col-lg-6">
					<div className="case-dark"></div>
				</div>
				<div className="col-lg-6">
					<div className="case">
						<h4>Lorem ipsum dolor</h4>
						<p>
							Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem
							ipsum
						</p>
						<button className="btn btn-secondary">Find forhandler</button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HomeShowcase;
