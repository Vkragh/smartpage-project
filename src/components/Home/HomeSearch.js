import { Search } from 'react-bootstrap-icons';

function HomeSearch() {
	return (
		<div className="container mt-5">
			<div className="row">
				<div className="col-lg-12 mb-5">
					<div className="home-search">
						<h4 className="mb-3">
							Find en trailer til <span>dine behov</span>
						</h4>
						<div className="options d-flex align-items-end">
							<div className="mb-2 me-2 option-wrapper">
								<label htmlFor="weight" className="form-label">
									Formål
								</label>
								<select
									className="form-select"
									aria-label="Default select example"
									defaultValue="Maskiner"
								>
									<option>Maskiner</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
								</select>
							</div>
							<div className="mb-2 me-2 option-wrapper">
								<label htmlFor="weight" className="form-label">
									Specielle funktioner
								</label>
								<select
									className="form-select"
									aria-label="Default select example"
									defaultValue="Køl / Frys"
								>
									<option>Køl / Frys</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
								</select>
							</div>
							<div className="mb-2 me-2 option-wrapper">
								<label htmlFor="weight" className="form-label">
									Totalvægt
								</label>
								<select
									className="form-select"
									aria-label="Default select example"
									name="weight"
									defaultValue="1000 kg"
								>
									<option>1000 kg</option>
									<option value="1">One</option>
									<option value="2">Two</option>
									<option value="3">Three</option>
								</select>
							</div>
							<div className="mb-2 me-2">
								<button className="btn btn-primary p-2 px-4">
									SØG <Search />
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HomeSearch;
