import { ArrowRight } from 'react-bootstrap-icons';

function HomeBanner() {
	return (
		<div className="container-fluid banner p-5">
			<div className="container">
				<div className="row">
					<div className="col-lg-5">
						<div className="banner-intro p-4">
							<h6>NYHED</h6>
							<h2 className="my-2">Pro-Line med bladfjedre</h2>
							<p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
							<button className="btn btn-secondary mt-3">
								Dummy link <ArrowRight />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default HomeBanner;
