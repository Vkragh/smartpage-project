import productImage from '../../images/trailer.png';
import { useState } from 'react';
import { ArrowRight } from 'react-bootstrap-icons';
import { Link } from 'react-router-dom';

function HomeProducts() {
	const [products, setProducts] = useState([
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
		'Fritidstrailere',
	]);
	return (
		<div className="container mt-5 mb-3 home-products">
			<div className="row">
				<div className="col-lg-2 mb-4">
					<div className="px-3 py-2 d-flex align-items-center disclaimer">
						<p>Vælg en Variant til arbejdet og få kvalitet på krogen</p>
					</div>
				</div>
				{products.map((product, index) => (
					<div className="col-lg-2 mb-4" key={index}>
						<div className="product p-2 ps-4 d-flex flex-column justify-content-between">
							<img src={productImage} alt="product" />
							<Link to="/products">
								{product} <ArrowRight />
							</Link>
						</div>
					</div>
				))}
			</div>
		</div>
	);
}

export default HomeProducts;
