import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { productSelector, fetchAllProducts } from '../../features/productSlice';

import ProductListItem from '../ProductList/ProductListItem';

function ProductList() {
	const { products, loading, totalProducts } = useSelector(productSelector);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchAllProducts());
	}, [dispatch]);

	return (
		<>
			{loading && <p>Loading..</p>}
			{!loading && products && (
				<div className="col-lg-12 pt-4 ps-3 products-list">
					<div className="products-title d-flex align-items-baseline">
						<h3 className="me-2">Products</h3>
						<small>({totalProducts} results)</small>
					</div>
					<hr />
					<p className="products-intro">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
						eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
						ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
						aliquip ex ea commodo consequat.
					</p>
					<div className="row mt-3">
						{products.map(product => (
							<ProductListItem product={product} key={product.Id} />
						))}
					</div>
				</div>
			)}
		</>
	);
}

export default ProductList;
