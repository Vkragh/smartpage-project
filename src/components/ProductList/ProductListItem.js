import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
	cartSelector,
	addProductToCart,
	fetchCart,
	updateCartItem,
} from '../../features/cartSlice';

function ProductListItem({ product }) {
	const dispatch = useDispatch();
	const { cart } = useSelector(cartSelector);

	const handleAddProduct = () => {
		const productExistsInCart = cart.OrderLines.find(
			item => item.ProductId === product.Id
		);

		if (productExistsInCart) {
			const productData = {
				id: product.Id,
				quantity: productExistsInCart.Quantity + 1,
			};
			dispatch(updateCartItem(productData)).then(prod => {
				if (prod.meta.requestStatus === 'fulfilled') {
					dispatch(fetchCart());
				}
			});
		} else {
			dispatch(addProductToCart(product)).then(prod => {
				if (prod.meta.requestStatus === 'fulfilled') {
					dispatch(fetchCart());
				}
			});
		}
	};

	return (
		<div className="col-lg-3 px-3">
			<div className="product-item rounded d-flex flex-column justify-content-between align-items-center text-center p-2 shadow-sm mb-2">
				<div className="product-image">
					<img
						alt="product"
						src={`http://rapido-343-full.websrv01.smartpage.dk/Files/Images/Ecom/Products/${product.Number}.jpg`}
					/>
				</div>
				<div className="pb-3 d-flex flex-column">
					<p className="product-name">{product.Name}</p>
					<small>{product.Number}</small>
					<p className="mb-4 product-price">
						{product.Price.Price}
						<span>$</span>
					</p>
					<div className="add-product">
						<Link
							to={`/product/${product.Id}`}
							className="btn btn-secondary me-2"
						>
							Læs mere
						</Link>
						<button className="btn btn-primary" onClick={handleAddProduct}>
							Tilføj til kurv
						</button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ProductListItem;
