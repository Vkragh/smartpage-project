import { Facebook, Youtube, Linkedin } from 'react-bootstrap-icons';

function Footer() {
	return (
		<div className="footer container-fluid p-3 pb-5">
			<div className="container">
				<div className="row">
					<div className="col-lg-3">
						<h5 className="mb-3 pb-3">Variant A/S</h5>
						<p>Godthåbstvej 5</p>
						<p>7100 Vejle</p>
						<p>Email: info@variant.dk</p>
						<p>CVR: 54 89 94 16</p>
					</div>
					<div className="col-lg-3 footer-nav">
						<h5 className="mb-3 pb-3">Hurtige genveje</h5>
						<p>&gt; Find forhandler</p>
						<p>&gt; Handelsbetingelser</p>
						<p>&gt; Cookie- og privatepolitik</p>
						<p>&gt; Sitemap</p>
					</div>
					<div className="col-lg-3 footer-nav">
						<h5 className="mb-3 pb-3">Udvalgte kategorier</h5>
						<p>&gt; Fritidstrailere - til hobbybrug</p>
						<p>&gt; Erhvervstrailere - Til den profesionelle håndværker</p>
						<p>&gt; Maxi-Load - de stærkeste trailere</p>
						<p>&gt; Categotrailere - Lukkede trailer</p>
						<p>&gt; Foliering - Få dit budskab ud</p>
					</div>
					<div className="col-lg-3 footer-social ps-4">
						<h5 className="pb-2">Sociale medier</h5>
						<div className="d-flex">
							<Facebook className="me-3" />
							<Youtube className="me-3" />
							<Linkedin />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Footer;
