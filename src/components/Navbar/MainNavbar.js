import React from 'react';
import NavbarCart from './NavbarCart';
import NavbarLogo from './NavbarLogo';
import NavbarSearch from './NavbarSearch';

function Navbar() {
	return (
		<div className="container-fluid navbar">
			<div className="container">
				<nav className="d-flex justify-content-between">
					<NavbarLogo />
					<NavbarSearch />
					<NavbarCart />
				</nav>
			</div>
		</div>
	);
}

export default Navbar;
