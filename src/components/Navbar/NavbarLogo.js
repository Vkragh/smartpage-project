import { Link } from 'react-router-dom';

function NavbarLogo() {
	return (
		<div className="logo">
			<h1 className="text-white">
				<Link to="/">Variant</Link>
			</h1>
		</div>
	);
}

export default NavbarLogo;
