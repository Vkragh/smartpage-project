import { Search } from 'react-bootstrap-icons';

function NavbarSearch() {
	return (
		<div className="nav-search d-flex">
			<input
				className="form-control"
				type="text"
				placeholder="Søg efter trailere, tilbehør og meget mere..."
			/>
			<div className="search-btn px-4 py-2">
				<Search />
			</div>
		</div>
	);
}

export default NavbarSearch;
