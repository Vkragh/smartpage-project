import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
	createNewCart,
	fetchCart,
	cartSelector,
} from '../../features/cartSlice';
import { Link } from 'react-router-dom';
import { CartCheck } from 'react-bootstrap-icons';

function NavbarCart() {
	const dispatch = useDispatch();
	const { totalPrice, productsCount } = useSelector(cartSelector);

	useEffect(() => {
		if (localStorage.getItem('cart_key') === null) {
			dispatch(createNewCart());
		} else {
			dispatch(fetchCart());
		}
	}, [dispatch]);

	return (
		<div className="nav-cart d-flex align-items-center justify-content-end">
			<div className="cart position-relative">
				<Link
					to="/cart"
					className="btn btn-primary d-flex align-items-center p-1 py-2"
				>
					<p className="px-2">{Math.round(totalPrice)} kr</p>
					<CartCheck />
				</Link>
				<div className="position-absolute cart-counter p-2">
					<p>{productsCount}</p>
				</div>
			</div>
		</div>
	);
}

export default NavbarCart;
