import { useState } from 'react';
import { Trash } from 'react-bootstrap-icons';

function CartListItem({ product }) {
	const [quantity, setQuantity] = useState(product.Quantity);

	const handleQuantityChange = e => {
		if (e.target.value >= 1) {
			setQuantity(e.target.value);
		}
	};

	return (
		<>
			<tr>
				<th>
					<img
						src={`http://rapido-343-full.websrv01.smartpage.dk/Files/Images/Ecom/Products/${product.ProductVariantId}.jpg`}
						alt="product"
					/>
				</th>
				<th>
					<p>{product.ProductVariantId}</p>
				</th>
				<th>
					<p>{product.ProductName}</p>
				</th>
				<th>
					<input
						className="form-control"
						type="number"
						value={quantity}
						onChange={handleQuantityChange}
					/>
				</th>
				<th>
					<div className="d-flex align-items-center justify-content-end">
						<p className="me-2">{product.Price.Price} kr</p>
						<Trash />
					</div>
				</th>
			</tr>
		</>
	);
}

export default CartListItem;
