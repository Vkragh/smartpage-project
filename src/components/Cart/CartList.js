import React from 'react';
import { useSelector } from 'react-redux';
import { cartSelector } from '../../features/cartSlice';
import CartListItem from './CartListItem';
import { ArrowRight } from 'react-bootstrap-icons';

function CartList() {
	const { cart, loading, totalPrice, productsCount } =
		useSelector(cartSelector);

	return (
		<>
			<div className="col-lg-12">
				{loading && <p>Loading..</p>}
				<div className="cart-header d-flex justify-content-between align-items-center my-3">
					<h3>Kurv</h3>
					<div>
						<button className="btn btn-secondary">Sign in & checkout</button>
						<small className="mx-2">or</small>
						<button className="btn btn-dark">
							Kassen <ArrowRight />
						</button>
					</div>
				</div>
				{cart && cart.OrderLines.length ? (
					<table className="table cart-table">
						<thead>
							<tr>
								<th></th>
								<th scope="col">Product number</th>
								<th scope="col">Navn</th>
								<th scope="col">Total antal</th>
								<th scope="col" className="text-end">
									Total pris inkl. moms
								</th>
							</tr>
						</thead>
						<tbody>
							{cart.OrderLines.map(product => (
								<CartListItem product={product} key={product.Id} />
							))}
						</tbody>
					</table>
				) : (
					<>
						<hr />
						<h5>Kurven er tom!</h5>
					</>
				)}
			</div>
			<div className="col-lg-6 offset-lg-6 cart-summary mt-5">
				<div className="d-flex justify-content-between">
					<p>Total antal</p>
					<p>{productsCount}</p>
				</div>
				<hr />
				<div className="d-flex justify-content-between total">
					<p>I alt</p>
					<p>{Math.round(totalPrice)} kr</p>
				</div>
			</div>
			<div className="col-lg-12 mt-5 d-flex justify-content-between">
				<button className="btn btn-secondary">Tøm kurven</button>
				<div>
					<button className="btn btn-secondary">Sign in & checkout</button>
					<small className="mx-2">or</small>
					<button className="btn btn-dark">
						Kassen <ArrowRight />
					</button>
				</div>
			</div>
		</>
	);
}

export default CartList;
