import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { fetchAllProducts } from '../../features/productSlice';
import { CartFill } from 'react-bootstrap-icons';

function MainProduct({ prodId }) {
	const dispatch = useDispatch();
	const history = useHistory();
	const [product, setProduct] = useState(null);

	useEffect(() => {
		dispatch(fetchAllProducts()).then(response => {
			const product = response.payload.Products.find(
				prod => prod.Id === prodId
			);
			if (product) {
				setProduct(product);
			} else {
				history.push('/products');
			}
		});
	}, [dispatch, history, prodId]);

	return (
		<>
			{product && (
				<div className="container product-info">
					<div className="row">
						<div className="col-lg-7 d-flex justify-content-center">
							<img
								src={`http://rapido-343-full.websrv01.smartpage.dk/Files/Images/Ecom/Products/${product.Number}.jpg`}
								alt="product"
							/>
						</div>
						<div className="col-lg-5 d-flex justify-content-center flex-column">
							<div className="product-desc">
								<h3>{product.Title}</h3>
								<small>Varenummer: {product.Number}</small>
							</div>
							<hr />
							<div className="product-price d-flex align-items-center mb-3">
								<p className="me-2">{product.Price.Price}</p>
								<span>$</span>
							</div>
							<button className="btn btn-primary text-center py-2">
								Læg i kurv <CartFill />
							</button>
						</div>
					</div>
				</div>
			)}
		</>
	);
}

export default MainProduct;
