const productApi = {
	async fetchAllProducts() {
		const res = await fetch(
			'http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/products?RepositoryName=Products&QueryName=Products',
			{
				method: 'POST',
				'Content-Type': 'application/x-www-form-urlencoded',
			}
		);
		const data = res.json();

		return data;
	},
};

export default productApi;
