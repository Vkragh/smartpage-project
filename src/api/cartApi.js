const cartApi = {
	async createCart() {
		const res = await fetch(
			'http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/carts?currencyCode=USD&countryCode=US&languageId=LANG1&additionalProp1=SystemName,string,Name,string,Type,string&additionalProp2=SystemName,string,Name,string,Type,string&additionalProp3=SystemName,string,Name,string,Type,string',
			{
				method: 'POST',
				'Content-Type': 'application/x-www-form-urlencoded',
			}
		);
		const data = res.json();
		return data;
	},
	async fetchCart(cartSecret) {
		const res = await fetch(
			`http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/carts/${cartSecret}`,
			{
				method: 'GET',
				'Content-Type': 'application/x-www-form-urlencoded',
			}
		);
		const data = res.json();
		return data;
	},
	async addItemToCart(cartSecret, product) {
		const productData = new URLSearchParams();
		productData.append('productId', product.Id);
		productData.append('quantity', 1);
		productData.append('productVariantId', product.Number);

		await fetch(
			`http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/carts/${cartSecret}/items`,
			{
				method: 'POST',
				'Content-Type': 'application/x-www-form-urlencoded',
				body: productData,
			}
		);
	},
	async updateCartItem(cartSecret, id, quantity) {
		const productData = new URLSearchParams();
		productData.append('quantity', quantity);

		await fetch(
			`http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/carts/${cartSecret}/items/${id}`,
			{
				method: 'PATCH',
				'Content-Type': 'application/x-www-form-urlencoded',
				body: productData,
			}
		);
	},
	async clearCart(cartSecret) {
		await fetch(
			`http://rapido-343-full.websrv01.smartpage.dk/dwapi/ecommerce/carts/${cartSecret}/items`,
			{
				method: 'DELETE',
				'Content-Type': 'application/x-www-form-urlencoded',
			}
		);
	},
};

export default cartApi;
