import React from 'react';
import CartList from '../components/Cart/CartList';
import Footer from '../components/Footer/Footer';
import MainNavbar from '../components/Navbar/MainNavbar';

function Cart() {
	return (
		<div className="flex-wrapper">
			<div>
				<MainNavbar />
				<div className="container my-5">
					<div className="row">
						<CartList />
					</div>
				</div>
			</div>
			<Footer />
		</div>
	);
}

export default Cart;
