import ProductList from '../components/ProductList/ProductList';
import MainNavbar from '../components/Navbar/MainNavbar';
import Footer from '../components/Footer/Footer';

function Products() {
	return (
		<div className="flex-wrapper">
			<MainNavbar />
			<div className="container mb-5">
				<div className="row">
					<ProductList />
				</div>
			</div>
			<Footer />
		</div>
	);
}

export default Products;
