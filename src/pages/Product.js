import Footer from '../components/Footer/Footer';
import MainNavbar from '../components/Navbar/MainNavbar';
import MainProduct from '../components/Product/MainProduct';

function Product({ match }) {
	return (
		<div className="flex-wrapper">
			<div>
				<MainNavbar />
				<MainProduct prodId={match.params.id} />
			</div>
			<Footer />
		</div>
	);
}

export default Product;
