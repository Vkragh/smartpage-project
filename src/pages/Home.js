import MainNavbar from '../components/Navbar/MainNavbar';
import Footer from '../components/Footer/Footer';
import HomeBanner from '../components/Home/HomeBanner';
import HomeSearch from '../components/Home/HomeSearch';
import HomeProducts from '../components/Home/HomeProducts';
import HomeShowcase from '../components/Home/HomeShowcase';

function Home() {
	return (
		<div className="flex-wrapper">
			<div>
				<MainNavbar />
				<HomeBanner />
				<HomeSearch />
				<HomeProducts />
				<HomeShowcase />
			</div>
			<Footer />
		</div>
	);
}

export default Home;
